import { Component } from 'react';

interface GridProps {
  gridSize: number;
  liveProbability: number;
  setCallables: (callables: { [index: string]: () => void }) => void;
}

interface GridState {
  grids: boolean[][][];
  interval?: NodeJS.Timeout | null;
}

export default class Grid extends Component<GridProps, GridState> {
  constructor(props: GridProps) {
    super(props);
    const { gridSize } = props;

    this.state = {
      grids: [Array(gridSize).fill(Array(gridSize).fill(false))],
    };
  }

  componentDidMount() {
    this.props.setCallables({
      clear: this.clear.bind(this),
      random: this.random.bind(this),
      pause: this.pause.bind(this),
      start: this.start.bind(this),
    });
  }

  clear() {
    const { gridSize } = this.props;
    this.setState({
      grids: [Array(gridSize).fill(Array(gridSize).fill(false))],
    });
  }

  random() {
    const { grids } = this.state;
    const { liveProbability } = this.props;
    const grid = grids[0].map((row) =>
      row.map(() => Math.random() > liveProbability),
    );
    this.setState({ grids: [grid] });
  }

  start() {
    if (this.state.interval) {
      return;
    }

    this.setState({ interval: setInterval(() => this.tick(), 1000) });
  }

  pause() {
    if (!this.state.interval) {
      return;
    }

    clearInterval(this.state.interval);
    this.setState({ interval: null });
  }

  tick() {
    const grids = this.state.grids.slice(0, 10);
    const curGrid = grids[0];
    const { gridSize } = this.props;

    const nextGrid = curGrid.map((col, x) =>
      col.map((cell, y) => {
        let neighborsAlive = 0;
        // Define deltas
        for (let dx = -1; dx <= 1; dx++) {
          const newX = x + dx;
          if (newX < 0 || newX >= gridSize) continue;
          for (let dy = -1; dy <= 1; dy++) {
            if (dy === 0 && dx === 0) continue;
            const newY = y + dy;
            if (newY < 0 || newY >= gridSize) continue;
            if (curGrid[newX][newY]) neighborsAlive++;
          }
        }

        // Rules:
        // Any live cell with fewer than 2 live neighbors dies: underpopulation
        // Any live cell with 2 or 3 live neighbors lives on: regular cells
        // Any live cell with more than 3 live neighbors dies: overpopulation
        // Any dead cell with exactly 3 live neighbors becomes a live cell: reproduction
        return neighborsAlive === 3 || (cell && neighborsAlive === 2);
      }),
    );
    grids.unshift(nextGrid);

    this.setState({ grids });
  }

  toggleCell(x: number, y: number) {
    if (this.state.interval) {
      return;
    }

    const grid = this.state.grids[0];
    this.setState({
      grids: [
        grid.map((col, i) =>
          col.map((cell, j) => (x === i && y === j ? !cell : cell)),
        ),
      ],
    });
  }

  render() {
    const grid = this.state.grids[0];

    return (
      <div className='flex'>
        {grid.map((col, x) => (
          <div key={`c${x}`} className='flex-1'>
            {col.map((cell, y) => (
              <div
                key={`(${x}, ${y})`}
                className={`border border-gray-400 ${
                  cell ? 'bg-gray-800 dark:bg-white' : ''
                }`}
                style={{ aspectRatio: '1' }}
                onClick={() => this.toggleCell(x, y)}
              />
            ))}
          </div>
        ))}
      </div>
    );
  }
}
